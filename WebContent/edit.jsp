<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Edit comments</title>
</head>
	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="errorMessage">
					<li><c:out value="${errorMessage}" />
				</c:forEach>
			</ul>
		</div>
	</c:if>
<body>
	<div class="main-contents">
		<div class="form-area">
				<form action="edit" method="post">
					<textarea name="text" cols="100" rows="5" class="tweet-box"><c:out value="${message.text}" /></textarea><br />
					<br />
					<input name="id" value="${message.id}" id="id" type="hidden" />
					 <input type="submit" value="更新">（140文字まで）<br />
					<br />
					<a href="./">戻る</a>
				</form>
		</div>
	</div>
	<div class="copyright">Copyright(c)Chiho Iwasaki</div>
</body>
</html>