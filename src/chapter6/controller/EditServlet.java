package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<String> errorMessages = new ArrayList<String>();
		HttpSession session = request.getSession();

		if (request.getParameter("id").isEmpty() ||request.getParameter("id").matches("^[^0-9]+$")) {
			errorMessages.add("不正なパラメータが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}

		int id = Integer.parseInt(request.getParameter("id"));

		MessageService messageService = new MessageService();
		Message message = messageService.select(id);

		if (message == null) {
			errorMessages.add("不正なパラメータが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}

		request.setAttribute("message", message);
		request.getRequestDispatcher("edit.jsp").forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		Message message = getMessage(request);
		List<String> errorMessages = new ArrayList<String>();

		if (isValid(message, errorMessages)) {
			try {
				new MessageService().update(message);
				response.sendRedirect("./");

			} catch (NoRowsUpdatedRuntimeException e) {
				errorMessages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
			}
		} else {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("message", message);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
			return;
		}

	}

	private Message getMessage(HttpServletRequest request) throws IOException, ServletException {

		Message message = new Message();
		message.setText(request.getParameter("text"));
		message.setId(Integer.parseInt(request.getParameter("id")));

		return message;
	}

	private boolean isValid(Message message, List<String> errorMessages) {

		String text = message.getText();

		if (StringUtils.isEmpty(text)) {
			errorMessages.add("メッセージを入力してください");

		} else if (140 < text.length()) {
			errorMessages.add("140文字以下で入力してください");

		} else if (StringUtils.isBlank(text)) {
			errorMessages.add("入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}
