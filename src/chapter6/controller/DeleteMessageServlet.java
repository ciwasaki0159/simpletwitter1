package chapter6.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.service.MessageService;


@WebServlet(urlPatterns = { "/deletemessage" })
public class DeleteMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String messageId = request.getParameter("id");
		int id = Integer.parseInt(messageId);

		MessageService messageService = new MessageService();
		messageService.delete(id);
		//メッセージをサービスのデリートメソッドを作って引数に（id）を渡す
		response.sendRedirect("./");


	}
}